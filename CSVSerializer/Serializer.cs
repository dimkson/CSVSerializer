﻿using System;
using System.Text;
using System.Reflection;

namespace CSVSerializer
{
    class Serializer: ISerialize
    {
        public string Serialize<T>(T obj)
        {
            var type = typeof(T);
            var propertyInfos = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var stringBuilder = new StringBuilder();

            foreach (PropertyInfo item in propertyInfos)
            {
                stringBuilder.Append($"{item.Name}:{item.GetValue(obj)};");
            }
            return stringBuilder.ToString();
        }

        public T Deserialize<T>(string csv) where T : new()
        {
            var properties = csv.Split(';', StringSplitOptions.RemoveEmptyEntries);

            T newObj = Activator.CreateInstance<T>();
            var type = newObj.GetType();
            
            foreach (string property in properties)
            {
                var propertyNameValue = property.Split(':');
                if (propertyNameValue.Length == 2)
                {
                    var propertyName = propertyNameValue[0];
                    var propertyValue = propertyNameValue[1];
                    var propertyInfo = type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);

                    propertyInfo.SetValue(newObj, Convert.ChangeType(propertyValue, propertyInfo.PropertyType));
                }
                else
                {
                    throw new FormatException("Неверный формат строки!");
                }
            }
            return newObj;
        }
    }
}
