﻿using System;
using System.Diagnostics;

namespace CSVSerializer
{
    /// <summary>
    /// Класс тестирует скорость сериализации и десериализации.
    /// </summary>
    static class SerializeTester
    {
        // Количество итераций теста.
        private const int COUNT = 1_000_000;

        /// <summary>
        /// Метод сериализует и десериализует объект и выводит затраченное время в консоль.
        /// </summary>
        /// <param name="obj">Объект, который необходимо протестировать.</param>
        /// <param name="serializer">Метод сериализации.</param>
        public static void TimeTest<T>(T obj, ISerialize serializer) where T : new()
        {
            var sw = new Stopwatch();
            string csv = string.Empty;

            Console.WriteLine($"Количество итераций: {COUNT}");
            try
            {
                sw.Start();
                for (var i = 0; i < COUNT; i++)
                {
                    csv = serializer.Serialize(obj);
                }
                sw.Stop();
                Console.WriteLine($"Сериализованная строка: {csv}");
                Console.WriteLine($"Время на сериализацию: {sw.Elapsed}");

                T newObj = default;
                sw.Restart();
                for (var i = 0; i < COUNT; i++)
                {
                    newObj = serializer.Deserialize<T>(csv);
                }
                sw.Stop();
                Console.WriteLine(newObj.ToString());
                Console.WriteLine($"Время на десериализацию: {sw.Elapsed}\n");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
    }
}
