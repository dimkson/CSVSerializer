﻿namespace CSVSerializer
{
    class Program
    {
        static void Main()
        {
            F f = F.Get();
            ISerialize serializer = new Serializer();
            SerializeTester.TimeTest(f, serializer);

            serializer = new StandartJsonSerializer();
            SerializeTester.TimeTest(f, serializer);
        }
    }
}
