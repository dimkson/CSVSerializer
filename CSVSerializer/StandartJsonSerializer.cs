﻿using System.Text.Json;

namespace CSVSerializer
{

    class StandartJsonSerializer : ISerialize
    {
        public T Deserialize<T>(string json) where T: new()
        {
            return JsonSerializer.Deserialize<T>(json);
        }

        public string Serialize<T>(T obj)
        {
            return JsonSerializer.Serialize<T>(obj);
        }
    }
}
