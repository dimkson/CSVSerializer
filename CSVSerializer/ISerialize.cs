﻿namespace CSVSerializer
{
    internal interface ISerialize
    {
        string Serialize<T>(T obj);
        T Deserialize<T>(string csv) where T : new()

    }
}